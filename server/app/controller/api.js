'use strict';

const Controller = require('egg').Controller;

class ApiController extends Controller {
  async comments() {
    const { ctx } = this;
    const data = await this.service.comments.getCommentsByDate();
    ctx.body = data;
  }
}

module.exports = ApiController;
