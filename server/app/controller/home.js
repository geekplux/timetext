'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    const isExist = await ctx.service.db.checkTable();
    console.info('table comments isExist: ', isExist);
    if (!isExist) await ctx.service.db.createTable();
    const hasData = await ctx.service.db.hasData();
    console.info('there is data in table comments: ', hasData);
    if (!hasData) await ctx.service.db.importData();
    ctx.body = 'hi, egg';
  }
}

module.exports = HomeController;
