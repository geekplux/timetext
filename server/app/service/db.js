'use strict';

// app/service/db.js
const Service = require('egg').Service;
const csv = require('fast-csv');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

class DBService extends Service {
  async checkTable() {
    const table = await this.app.mysql.query('show tables');
    return !!table.length;
  }

  async hasData() {
    const data = await this.app.mysql.get('comments', { id: 1 });
    return !!data;
  }

  async createTable() {
    const sql = `
    CREATE TABLE \`comments\` (
      \`id\` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
      \`Symbol\` varchar(255) DEFAULT NULL,
      \`PROs\` varchar(5000) DEFAULT NULL,
      \`CONs\` varchar(5000) DEFAULT NULL,
      \`Date\` datetime DEFAULT NULL,
      \`PROs_Management\` varchar(255) DEFAULT NULL,
      \`PROs_Career_Opportunities\` varchar(255) DEFAULT NULL,
      \`PROs_Comp_Benefits\` varchar(255) DEFAULT NULL,
      \`PROs_Work_Life_Balance\` varchar(255) DEFAULT NULL,
      \`PROs_Culture_Value\` varchar(255) DEFAULT NULL,
      \`PROs_Innovation_Technology\` varchar(255) DEFAULT NULL,
      \`CONs_Management\` varchar(255) DEFAULT NULL,
      \`CONs_Career_Opportunities\` varchar(255) DEFAULT NULL,
      \`CONs_Comp_Benefits\` varchar(255) DEFAULT NULL,
      \`CONs_Work_Life_Balance\` varchar(255) DEFAULT NULL,
      \`CONs_Culture_Value\` varchar(1024) DEFAULT NULL,
      \`CONs_Innovation_Technology\` varchar(255) DEFAULT NULL,
      PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='comments';
    `;

    try {
      await this.app.mysql.query(sql);
    } catch (error) {
      console.error('ERROR:  ', error);
    }
  }

  async importData() {
    importCsvData2MySQL(
      path.resolve(__dirname, '../../../sample.csv'),
      this.app.mysql
    );
  }
}

module.exports = DBService;

function importCsvData2MySQL(filename, mysql) {
  const stream = fs.createReadStream(filename);
  const csvData = [];
  const csvStream = csv
    .parse()
    .on('data', function(data) {
      data[3] = moment(data[3]).format('YYYY-MM-DD HH:mm:SS');
      csvData.push(data);
    })
    .on('end', async function() {
      // Remove Header ROW
      csvData.shift();

      const query =
        'INSERT INTO comments (Symbol, PROs, CONs, Date, PROs_Management, PROs_Career_Opportunities, PROs_Comp_Benefits, PROs_Work_Life_Balance, PROs_Culture_Value, PROs_Innovation_Technology, CONs_Management, CONs_Career_Opportunities, CONs_Comp_Benefits, CONs_Work_Life_Balance, CONs_Culture_Value, CONs_Innovation_Technology) VALUES ?';
      await mysql.query(query, [ csvData ]);
    });

  stream.pipe(csvStream);
}
