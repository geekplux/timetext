'use strict';

// app/service/db.js
const Service = require('egg').Service;
const moment = require('moment');

class CommentsService extends Service {
  async getCommentsByDate(date) {
    const data = await this.app.mysql.select('comments', {
      orders: [[ 'Date', 'ASC' ]],
    });

    // let minDate,
    //   maxDate;
    // data.map(d => {
    //   minDate = moment.min(d.Date);
    //   maxDate = moment.max(d.Date);
    // });

    // console.log(minDate, maxDate);

    return data;
  }
}

module.exports = CommentsService;
