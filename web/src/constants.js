export const prosKeyMap = [
  'PROs_Career_Opportunities',
  'PROs_Comp_Benefits',
  'PROs_Culture_Value',
  'PROs_Innovation_Technology',
  'PROs_Management',
  'PROs_Work_Life_Balance'
];

export const consKeyMap = [
  'CONs_Career_Opportunities',
  'CONs_Comp_Benefits',
  'CONs_Culture_Value',
  'CONs_Innovation_Technology',
  'CONs_Management',
  'CONs_Work_Life_Balance'
];

export const commentValueMap = [
  'Career Opportunities',
  'Comp Benefits',
  'Culture Value',
  'Innovation Technology',
  'Management',
  'Work Life Balance'
];

export const commonColor = '#9ebcda';
export const negColor = '#e0ecf4';
export const posColor = '#8856a7';