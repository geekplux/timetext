import React from 'react';
import Side from './containers/Side';
import Content from './containers/Content';
import './App.css';

function App() {
  return (
    <div className="App">
      <Side />
      <Content />
    </div>
  );
}

export default App;
