import moment from 'moment';
import * as _ from 'lodash';
import { prosKeyMap, consKeyMap, commentValueMap } from '../constants';

export const transformDate = data => {
  let _data = _.cloneDeep(data);
  _data = _.map(_data, d => {
    d.Date = moment(d['As Of Date']).format('YYYY-MM-DD HH:mm:SS');
    return d;
  })

  return _data;
}

export const groupDataByYear = data => {
  let _data = _.cloneDeep(data);
  _data = _.groupBy(
    _.map(_data, d => {
      d.Date = moment(d.Date).year();
      return d;
    }),
    'Date'
  );

  const results = [];
  _.map(_data, (d, key) => {
    let prosCount = 0;
    let consCount = 0;
    const result = {
      year: key,
      commentsCount: d.length
    };

    _.map(d, _d => {
      prosKeyMap.map((k, i) => {
        const r = _.words(_d[k]);
        if (!result[commentValueMap[i]]) {
          result[commentValueMap[i]] = { pro: 0, con: 0, sum: 0 };
        }
        result[commentValueMap[i]].pro += r.length;
        result[commentValueMap[i]].sum += r.length;
        prosCount += r.length;
      });
      consKeyMap.map((k, i) => {
        const r = _.words(_d[k]);
        result[commentValueMap[i]].con += r.length;
        result[commentValueMap[i]].sum += r.length;
        consCount += r.length;
      });
    });

    result.prosCount = prosCount;
    result.consCount = consCount;
    result.sumCount = prosCount + consCount;

    results.push(result);
  });

  return results;
};

export const groupIndicators = (data, year, type) => {
  let _data = _.cloneDeep(data);
  let results = [[], []];
  commentValueMap.map(c => {
    results[0].push({
      axis: c,
      type: 'pro',
      value: 0
    });
    results[1].push({
      axis: c,
      type: 'con',
      value: 0
    });
  });

  if (year) {
    _data = _.filter(_data, d => +year === +moment(d.Date).year());
  }

  _.map(_data, d => {
    prosKeyMap.map((k, i) => {
      const words = _.words(d[k]);
      results[0][i].value += words.length;
    });
    consKeyMap.map((k, i) => {
      const words = _.words(d[k]);
      results[1][i].value += words.length;
    });
  });

  return results;
};

export const groupTags = (data, year, type) => {
  let _data = _.cloneDeep(data);
  let results = {};
  let maxSum = -Infinity;
  let minSum = +Infinity;

  if (year) {
    _data = _.filter(_data, d => +year === +moment(d.Date).year());
  }

  _.map(_data, d => {
    prosKeyMap.map((k, i) => {
      const words = _.words(d[k]);
      words.map(w => {
        if (results[w]) {
          results[w].value.sum += 1;
          results[w].value.pro += 1;
        } else {
          results[w] = {
            tag: w,
            value: {
              pro: 1,
              sum: 1,
              con: 0
            },
            ids: []
          };
        }
        if (maxSum < results[w].value.sum) {
          maxSum = results[w].value.sum;
        }
        if (minSum > results[w].value.sum) {
          minSum = results[w].value.sum;
        }
        results[w].ids.push(d.id);
      });
    });
    consKeyMap.map((k, i) => {
      const words = _.words(d[k]);
      words.map(w => {
        if (results[w]) {
          results[w].value.sum += 1;
          results[w].value.con += 1;
        } else {
          results[w] = {
            tag: w,
            value: {
              pro: 0,
              con: 1,
              sum: 1
            },
            ids: []
          };
        }
        if (maxSum < results[w].value.sum) {
          maxSum = results[w].value.sum;
        }
        if (minSum > results[w].value.sum) {
          minSum = results[w].value.sum;
        }
        results[w].ids.push(d.id);
      });
    });
  });

  results = _.values(results);
  results.maxSum = maxSum;
  results.minSum = minSum;
  return results;
};

export const filterDataByTag = (data, d) => {
  let _data = _.cloneDeep(data);
  return _.filter(_data, _d => _.includes(d.ids, _d.id));
};
