import axios from 'axios';

const instance = axios.create({
  // baseURL: 'http://localhost:7001/api/comments',
  headers: {
    //   'Content-type': 'application/json'
    'content-type': 'application/json; charset=utf-8'
  },
  baseURL: 'https://express.geekplux.now.sh',
  timeout: 10000
});

export default instance;
