import React, { setGlobal } from 'reactn';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { transformDate, groupDataByYear, groupIndicators, groupTags } from './utils/formatData';
import originData from './data.json';

async function getComments() {
  try {
    const data = transformDate(originData);
    const dataGroupedByYear = groupDataByYear(data);
    const indicators = groupIndicators(data);
    const tags = groupTags(data);
    setGlobal({
      data,
      dataGroupedByYear,
      indicators,
      tags,
      year: null,
    });
  } catch (error) {
    console.error(error);
  }
}

getComments();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
