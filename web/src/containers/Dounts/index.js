import React, { useState } from 'react';
import { commentValueMap } from '../../constants';
import Dount from '../../charts/Dount';
import { negColor, posColor, commonColor } from '../../constants';
import * as d3 from 'd3';
import './style.css';

const radius = [1.5, 11];

function Dounts({ data, year }) {
  const [mode, setMode] = useState('Pros');

  let max = 0;
  data.map(d => {
    commentValueMap.map(k => {
      if (max < d[k].sum) max = d[k].sum;
    });
  });

  const y = d3
    .scaleLinear()
    .domain([0, max])
    .range(radius);

  function changeMode(event) {
    setMode(event.target.value);
  }

  return (
    <div className="Dounts">
      <form className="radio">
        <label>
          <input
            onChange={changeMode}
            type="radio"
            name="radio"
            value="Pros"
            checked={mode === 'Pros'}
          />
          Pros
        </label>
        <label>
          <input
            onChange={changeMode}
            type="radio"
            name="radio"
            value="Cons"
            checked={mode === 'Cons'}
          />
          Cons
        </label>
      </form>
      <div className="guide">
        <div>
          <span
            className="label"
            style={{
              background: posColor
            }}
          />{' '}
          Pros comments score
        </div>
        <div>
          <span
            className="label"
            style={{
              background: negColor
            }}
          />{' '}
          Cons comments score
        </div>
        <div>
          <span
            className="label"
            style={{
              background: commonColor
            }}
          />{' '}
          All comments score
        </div>
      </div>
      <ul className="list">
        {commentValueMap.map((c, i) => (
          <li className="li" key={i}>
            <h5>{c}</h5>
            <div className="squares">
              {Object.keys(data).map((idx, j) => (
                <div key={j} className="square">
                  <Dount
                    data={data[j][c]}
                    keyIdx={c}
                    mode={mode}
                    yScale={y}
                    dataYear={+data[j].year}
                    year={year}
                  />
                </div>
              ))}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Dounts;
