import React, { useState, useGlobal } from 'reactn';
import Stacked from '../../charts/Stacked';
import Dounts from '../Dounts';
import { negColor, posColor, commonColor } from '../../constants';
import './style.css';

function Side() {
  const [dy, setDy] = useGlobal('dataGroupedByYear');
  const [year, setYear] = useGlobal('year');
  const [layout, setLayout] = useState('stacked');

  function changeLayout(event) {
    setLayout(event.target.value);
  }

  return (
    <div className="Side">
      <div className="trending">
        <h2>Trending</h2>
        <form className="radio">
          <label>
            <input
              onChange={changeLayout}
              type="radio"
              name="radio"
              value="stacked"
              checked={layout === 'stacked'}
            />
            Stacked
          </label>
          <label>
            <input
              onChange={changeLayout}
              type="radio"
              name="radio"
              value="grouped"
              checked={layout === 'grouped'}
            />
            Grouped
          </label>
        </form>
        <div className="guide">
          <div>
            <span
              className="label"
              style={{
                background: posColor
              }}
            />{' '}
            Pros comments count
          </div>
          <div>
            <span
              className="label"
              style={{
                background: negColor
              }}
            />{' '}
            Cons comments count
          </div>
          <div>
            <span
              className="label"
              style={{
                background: commonColor
              }}
            />{' '}
            comments count trending
          </div>
        </div>
        {dy && dy.length && <Stacked layout={layout} data={dy} year={year} setYear={setYear} />}
      </div>
      {dy && dy.length && <Dounts data={dy} year={year} />}
    </div>
  );
}

export default Side;
