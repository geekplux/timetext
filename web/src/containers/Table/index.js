import React, { useGlobal } from 'reactn';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Highlighter from 'react-highlight-words';
import './style.css';

function Table() {
  const [data, setData] = useGlobal('data');
  const [tag, setTag] = useGlobal('tag');
  const [dataFilteredByTag, setDataFilteredByTag] = useGlobal(
    'dataFilteredByTag'
  );

  const columns = [
    {
      Header: 'Id',
      accessor: 'id', // String-based value accessors!
      width: 50
    },
    {
      Header: 'Date',
      accessor: 'Date',
      Cell: props => <span>{props.value.split('T')[0]}</span>,
      width: 120
    },
    {
      Header: 'Pros',
      accessor: 'PROs', // Custom value accessors!
      Cell: props => (
        <Highlighter
          highlightClassName="highlightWords"
          searchWords={[tag]}
          autoEscape={true}
          textToHighlight={props.value}
        />
      )
    },
    {
      Header: 'Cons',
      accessor: 'CONs',
      Cell: props => (
        <Highlighter
          highlightClassName="highlightWords"
          searchWords={[tag]}
          autoEscape={true}
          textToHighlight={props.value}
        />
      )
    }
  ];

  if (!dataFilteredByTag && !data) return null;
  return (
    <ReactTable
      sorted={[
        {
          // the sorting model for the table
          id: 'Date',
          desc: true
        },
        {
          id: 'id',
          asc: true
        }
      ]}
      className="Table"
      data={dataFilteredByTag ? dataFilteredByTag : data}
      columns={columns}
    />
  );
}

export default Table;
