import React, { useGlobal } from 'reactn';
import Table from '../Table';
import './style.css';
import Bubbles from '../../charts/Bubbles';
import Radar from '../../charts/Radar';
import {
  groupIndicators,
  groupTags,
  filterDataByTag
} from '../../utils/formatData';

let oldYear;
function Content() {
  const [data, setData] = useGlobal('data');
  const [indicators, setIndicators] = useGlobal('indicators');
  const [tags, setTags] = useGlobal('tags');
  const [year, setYear] = useGlobal('year');
  const [tag, setTag] = useGlobal('tag');
  const [dataFilteredByTag, setDataFilteredByTag] = useGlobal(
    'dataFilteredByTag'
  );

  function setFilterData(d) {
    setTag(d.tag);
    setDataFilteredByTag(filterDataByTag(data, d));
  }

  if (year !== oldYear) {
    setIndicators(groupIndicators(data, year));
    setTags(groupTags(data, year));
    oldYear = year;
  }

  return (
    <div className="Content">
      <h1>Glassdoor comments visualization</h1>
      <div className="inter">
        <p>
          Current selected year:
          <span>{year ? year : '2008-2019'}</span>
        </p>
        <p>
          Current selected word tag:
          <span>{tag ? tag : 'none'}</span>
        </p>
        <p>
          Count of filterd comments:
          <span>
            {dataFilteredByTag && dataFilteredByTag.length
              ? dataFilteredByTag.length
              : 0}
          </span>
        </p>
      </div>
      <Table />
      <div className="bottom">
        <Radar data={indicators} />
        <Bubbles data={tags} setData={setFilterData} />
      </div>
    </div>
  );
}

export default Content;
