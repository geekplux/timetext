import React from 'react';
import * as d3 from 'd3';
import './style.css';
import RadarChart from './radarChart';
import { posColor, negColor } from '../../constants';

const width = 200;
const height = 200;
const margin = { top: 50, right: 50, bottom: 60, left: 50 };

function renderChart(SVG, data) {
  if (!SVG || !data) return;

  const color = d => (d.type === 'pro' ? posColor : negColor);

  const radarChartOptions = {
    w: width,
    h: height,
    margin: margin,
    maxValue: 0.5,
    levels: 5,
    roundStrokes: true,
    color: color
  };
  //Call function to draw the Radar chart
  RadarChart(SVG, data, radarChartOptions);
}

class Radar extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    renderChart(this.ref.current, this.props.data);
  }

  componentWillReceiveProps(next, old) {
    renderChart(this.ref.current, next.data);
  }

  render() {
    return <div className="Radar" ref={this.ref} />;
  }
}

export default Radar;
