import React from 'react';
import * as d3 from 'd3';
import { posColor, negColor } from './../../constants';
import './style.css';
import tippy from 'tippy.js';

const width = 550;
const height = 320;

const rmin = 2;
const rmax = 50;
const x = d3.scaleLinear().range([rmin, rmax]);

let gs, pies, rect1, rect2, simulation, txts;
function renderChart(SVG, data, refresh, setData) {
  if (!SVG || !data) return;
  if (refresh) {
    gs && gs.remove();
    gs = null;
    pies && pies.remove()
    pies = null;
    rect1 && rect1.remove()
    rect1 = null;
    rect2 && rect2.remove()
    rect2 = null;
    simulation = null;
    txts = null;
  }

  const svg = d3.select(SVG);
  svg
    .attr('width', width)
    .attr('height', height)
    .attr('class', 'svg');

  x.domain([data.minSum, data.maxSum]);

  if (!simulation) {
    simulation = d3
      .forceSimulation(data)
      .velocityDecay(0.2)
      .alphaDecay(0.005)
      .force('center', d3.forceCenter(width / 2, height / 2))
      .force('x', d3.forceX().strength(0.002))
      .force('y', d3.forceY().strength(0.002))
      .force(
        'collide',
        d3
          .forceCollide()
          .radius(function(d) {
            d.r = x(d.value.sum);
            return d.r + 0.5;
          })
          .iterations(2)
      )
      .on('tick', ticked);
  }

  if (!gs) {
    gs = svg
      .selectAll('g')
      .data(data)
      .enter()
      .append('g');
  }

  if (!pies) {
    pies = gs
      .append('g')
      .attr('class', 'pie')
      .attr('class', 'pieg')
      .attr('data-tippy-content', d => {
        return `word: ${d.tag}, pro: ${d.value.pro}, con: ${d.value.con}`;
      });

    pies
      .on('mouseover', function(d) {
        pies.attr('fill-opacity', 0.1);
        d3.select(this).attr('fill-opacity', 1);
      })
      .on('mouseout', function() {
        pies.attr('fill-opacity', 1);
      })
      .on('click', function(d) {
        setData(d);
      });

    setTimeout(function() {
      tippy('.pieg');
    }, 1000);
  }

  if (!rect1 || !rect2) {
    pies
      .append('clipPath')
      .attr('id', (d, i) => `id${i}`)
      .append('circle')
      .attr('r', d => d.r);

    rect1 = pies
      .append('rect')
      .attr('width', d => d.r * 2)
      .attr('height', d => d.r * 2)
      .attr('x', d => -d.r)
      .attr('y', d => -d.r)
      .attr('fill', negColor)
      .attr('clip-path', (d, i) => `url(#id${i})`);
    rect2 = pies
      .append('rect')
      .attr('width', d => d.r * 2)
      .attr('height', d => d.r * 2)
      .attr('x', d => -d.r + (d.value.con / d.value.sum) * (2 * d.r))
      .attr('y', d => -d.r)
      .attr('fill', posColor)
      .attr('clip-path', (d, i) => `url(#id${i})`);
  }

  if (!txts) {
    txts = gs
      .filter(d => d.r > 15)
      .append('text')
      .text(d => d.tag);
  }

  function ticked() {
    if (!gs) return;
    gs.attr(
      'transform',
      d =>
        `translate(${Math.max(d.r, Math.min(width - d.r, d.x))}, ${Math.max(
          d.r,
          Math.min(height - d.r, d.y)
        )})`
    );
  }
}

class Bubbles extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    renderChart(this.ref.current, this.props.data, false, this.props.setData);
  }

  componentWillReceiveProps(next, old) {
    renderChart(this.ref.current, next.data, true, next.setData);
  }

  render() {
    return (
      <div className="Bubbles">
        <svg ref={this.ref} />
        {this.props.data && (
          <p>
            Total of tags is:
            <span>{this.props.data.length}</span>
          </p>
        )}
      </div>
    );
  }
}

export default Bubbles;
