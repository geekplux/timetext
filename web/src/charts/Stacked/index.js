import React from 'react';
import * as d3 from 'd3';
import tippy from 'tippy.js';
import { posColor, negColor, commonColor } from '../../constants';
import './style.css';

const margin = { top: 0, right: 0, bottom: 60, left: 30 };
const width = 350;
const height = 300;
const n = 2;

let rect;
let xAxis;
let yAxis;
let xAxisR;
let yAxisR;
let path;

function renderChart(SVG, layout, data, year, setYear) {
  const stack = d3.stack().keys(['prosCount', 'consCount']);
  const series = stack(data);

  let yMax = 0;
  let y1Max = 0;
  data.map(d => {
    if (yMax < d.prosCount) yMax = d.prosCount;
    if (yMax < d.consCount) yMax = d.consCount;
  });
  series[1].map(d => {
    if (y1Max < d[1]) y1Max = d[1];
  });
  series.map(s => {
    s.map(ss => ss.push(s.index));
  });

  const x = d3
    .scaleBand()
    .domain(Object.keys(data))
    .rangeRound([margin.left, width - margin.right])
    .padding(0.08);
  const y = d3
    .scaleLinear()
    .domain([0, y1Max])
    .range([height - margin.bottom, margin.top]);

  const svg = d3.select(SVG);
  svg.attr('width', width).attr('height', height);

  xAxis = svg =>
    svg
      .append('g')
      .attr('transform', `translate(0,${height - margin.bottom})`)
      .call(
        d3
          .axisBottom(x)
          .tickSizeOuter(0)
          .tickPadding(30)
          .tickFormat(i => data[i].year)
      );
  yAxis = g =>
    g
      .attr('transform', `translate(${margin.left},0)`)
      .call(d3.axisLeft(y))
      .call(g => g.select('.domain').remove());

  const line = d3
    .line()
    .x((d, i) => x(i))
    .y(d => y(d.commentsCount));

  if (!rect) {
    rect = svg
      .selectAll('g')
      .data(series)
      .enter()
      .append('g')
      .attr('fill', d => (d.key === 'consCount' ? negColor : posColor))
      .selectAll('rect')
      .data(d => d)
      .join('rect')
      .attr('class', 'bar')
      .attr('x', (d, i) => x(i))
      .attr('y', height - margin.bottom)
      .attr('width', x.bandwidth())
      .attr('height', 0)
      .attr('data-tippy-content', d => {
        return d[2]
          ? `${d.data.year}, con: ${d.data.consCount}`
          : `${d.data.year}, pro: ${d.data.prosCount}`;
      });

    rect
      .on('mouseover', function(d) {
        rect.classed('blur', true);
        d3.select(this).classed('blur', false);
      })
      .on('mouseout', function(d) {
        rect.classed('blur', false);
      })
      .on('click', function(d) {
        const dom = d3.select(this);
        if (dom.classed('selected')) {
          rect.classed('selected', false)
          setYear(null);
        } else {
          rect.classed('selected', false)
          dom.classed('selected', true);
          setYear(d.data.year)
        }
      });

    setTimeout(function() {
      tippy('.bar');
    }, 1000);
  }

  if (!path) {
    path = svg
      .append('path')
      .datum(data)
      .attr('fill', 'none')
      .attr('stroke', commonColor)
      .attr('stroke-width', 1.5)
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr('d', line);
  }

  function transitionGrouped(rect) {
    y.domain([0, yMax]);
    yAxis = g =>
      g
        .attr('transform', `translate(${margin.left},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select('.domain').remove());

    yAxisR.call(yAxis);
    rect
      .transition()
      .duration(500)
      .delay((d, i) => i * 20)
      .attr('x', (d, i) => x(i) + (x.bandwidth() / n) * d[2])
      .attr('width', x.bandwidth() / n)
      .transition()
      .attr('y', d => y(d[1] - d[0]))
      .attr('height', d => y(0) - y(d[1] - d[0]));
  }

  function transitionStacked(rect) {
    y.domain([0, y1Max]);
    yAxis = g =>
      g
        .attr('transform', `translate(${margin.left},0)`)
        .call(d3.axisLeft(y))
        .call(g => g.select('.domain').remove());

    yAxisR.call(yAxis);
    rect
      .transition()
      .duration(500)
      .delay((d, i) => i * 20)
      .attr('y', d => y(d[1]))
      .attr('height', d => y(d[0]) - y(d[1]))
      .transition()
      .attr('x', (d, i) => x(i))
      .attr('width', x.bandwidth());
  }

  if (!xAxisR)
    xAxisR = svg
      .append('g')
      .attr('class', 'xAxis')
      .call(xAxis);
  if (!yAxisR)
    yAxisR = svg
      .append('g')
      .attr('class', 'yAxis')
      .call(yAxis);

  if (layout === 'stacked') transitionStacked(rect);
  else transitionGrouped(rect);
}

class Stacked extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    const { layout, data, year, setYear } = this.props;
    renderChart(this.ref.current, layout, data, year, setYear);
  }

  componentWillReceiveProps(next, old) {
    const { layout, data, year, setYear } = next;
    if (layout !== old.layout || old.year !== year)
      renderChart(this.ref.current, layout, data, year, setYear);
  }

  render() {
    return (
      <div className="Stacked">
        <svg ref={this.ref} />
      </div>
    );
  }
}

export default Stacked;
