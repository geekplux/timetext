import React from 'react';
import * as d3 from 'd3';
import tippy from 'tippy.js';
import { posColor, negColor, commonColor } from '../../constants';
import './style.css';

const width = 23;
const height = 23;

function renderChart(SVG, mode, data, yScale, key, year, dataYear) {
  const svg = d3.select(SVG);
  svg
    .attr('width', width)
    .attr('height', height)
    .attr('class', 'svg');
  svg.selectAll('circle').remove();

  svg
    .append('g')
    .attr('class', 'bg')
    .append('circle')
    .datum(data)
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('class', 'dount')
    .style('transform', 'translate(50%, 50%)')
    .attr('fill', +year === +dataYear ? 'yellowgreen' : commonColor)
    .attr('r', d => yScale(d.sum))
    .attr('data-tippy-content', d => `all comments score: ${d.sum}`);

  const pro = svg
    .append('circle')
    .datum(data)
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('class', 'dount')
    .style('transform', 'translate(50%, 50%)')
    .attr('fill', +year === +dataYear ? 'yellow' : posColor)
    .attr(
      'data-tippy-content',
      d => `score of pro comments about ${key}: ${d.pro}`
    );

  const con = svg
    .append('circle')
    .datum(data)
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('class', 'dount')
    .style('transform', 'translate(50%, 50%)')
    .attr('fill', +year === +dataYear ? 'green': negColor)
    .attr(
      'data-tippy-content',
      d => `score of con comments about ${key}: ${d.con}`
    );

  if (mode === 'Pros') {
    pro
      .transition()
      .duration(500)
      .attr('r', d => yScale(d.pro));
  } else {
    con
      .transition()
      .duration(500)
      .attr('r', d => yScale(d.con));
  }

  const dounts = svg.selectAll('.dount');
  dounts
    .on('mouseover', function(d) {
      d3.select('.Dounts')
        .selectAll('.dount')
        .attr('fill-opacity', 0.2);
      d3.select(this).attr('fill-opacity', 1);
    })
    .on('mouseout', function(d) {
      d3.select('.Dounts')
        .selectAll('.dount')
        .attr('fill-opacity', 1);
    })
    .on('click', function(d) {
      console.log('click', d);
    });

  setTimeout(function() {
    tippy('.dount');
  }, 1000);
}

class Dount extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    const { data, mode, yScale, keyIdx, year, dataYear } = this.props;
    renderChart(this.ref.current, mode, data, yScale, keyIdx, year, dataYear);
  }

  componentWillReceiveProps(next, old) {
    const { data, mode, yScale, keyIdx, year, dataYear } = next;
    renderChart(this.ref.current, mode, data, yScale, keyIdx, year, dataYear);
  }

  render() {
    return (
      <div className="Dount">
        <svg ref={this.ref} />
      </div>
    );
  }
}

export default Dount;
